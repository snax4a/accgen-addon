var HEADERS_TO_STRIP_LOWERCASE = [
	'content-security-policy',
	'x-frame-options',
	'access-control-allow-origin'
];

function setHeader(e) {
	var origin = new URL((e.originUrl || e.initiator)).origin;
	if (!origin.endsWith("cathook.club"))
		return;
	if (!e.responseHeaders)
		return;

	e.responseHeaders = e.responseHeaders.filter(function (header) {
		return HEADERS_TO_STRIP_LOWERCASE.indexOf(header.name.toLowerCase()) < 0;
	});

	var url = !(e.originUrl || e.initiator) ? new URL("https://accgen.cathook.club") : new URL(e.originUrl || e.initiator);
	e.responseHeaders.push({
		name: "Access-Control-Allow-Origin",
		value: url.origin
	});
	e.responseHeaders.push({
		name: "Access-Control-Allow-Credentials",
		value: "true"
	});

	return {
		responseHeaders: e.responseHeaders
	};
}

var browser_api = chrome ? chrome : browser_api;

browser_api.webRequest.onHeadersReceived.addListener(setHeader, {
	urls: ["https://store.steampowered.com/*", "https://mail.google.com/mail/feed/*"]
}, (typeof browser != "undefined" ? ["blocking", "responseHeaders"] : ["blocking", "responseHeaders", "extraHeaders"]));

var timeout_email_automation = null;
var emailAutomationData = {};

function emailAutomationDone(response) {
	if (timeout_email_automation)
		clearTimeout(timeout_email_automation);
	timeout_email_automation = null;
	if (typeof browser == "undefined")
		emailAutomationData.port.postMessage({ forcs: true, task: "closeWindow" });
	emailAutomationData.port.postMessage({ id: emailAutomationData.id, data: response });
	//emailAutomationData.doneResponse({ id: emailAutomationData.id, data: response });
	emailAutomationData = null;
}

function emailAutomationTimedOut() {
	emailAutomationDone({ success: false, reason: "Email automation timed out! Are you signed into gmail?" });
}

function extendTimeout(time) {
	if (timeout_email_automation)
		clearTimeout(timeout_email_automation);
	timeout_email_automation = setTimeout(emailAutomationTimedOut, time);
}

// Handle long-term comms with accgen content scripts
browser_api.runtime.onConnect.addListener(function (port) {
	port.onMessage.addListener(function (request) {
		switch (request.data.task) {
			case "setupGmail":
				extendTimeout(20000);
				emailAutomationData = {};
				emailAutomationData.port = port;
				emailAutomationData.id = request.id;
				browser_api.storage.local.set({ setGmail: 1, timeout: Date.now() + 5 * 1000 }, function () {
					if (typeof browser != "undefined")
						browser.windows.create({ url: "https://mail.google.com/mail/?ui=html&zy=h&v=prufw", allowScriptsToClose: true, state: "minimized" });
					else
						port.postMessage({ forcs: true, task: "openWindow" })
				});
				return true;
			case "version":
				// Tell the website about our version, may be important for backwards compat in the future
				port.postMessage({ id: request.id, data: { version: browser_api.runtime.getManifest().version } });
				break;
			default:
				break;
		}
	});
});

browser_api.runtime.onMessage.addListener(function (request, sender, sendResponse) {
	var senderOrigin = new URL(sender.url).origin;
	// Handle requests from gmail content script.
	if (senderOrigin == "https://mail.google.com") {
		if (!timeout_email_automation)
			return;
		sendResponse("ok");
		emailAutomationDone({
			success: request.success ? true : false,
			reason: typeof request.reason != "string" ? (!request.success ? "Unknown error!" : undefined) : request.reason,
			email: typeof request.email != "string" ? undefined : request.email
		});
	}
});
