var browser_api = chrome ? chrome : browser_api;

var port = browser_api.runtime.connect({ name: "port-from-cs" });

document.addEventListener('addon', function (event) {
    port.postMessage(event.detail)
});

var gmailwindow = null;

port.onMessage.addListener(function (reply) {
    if (reply.forcs) {
        switch (reply.task) {
            case "openWindow":
                    gmailwindow = window.open("https://mail.google.com/mail/?ui=html&zy=h&v=prufw");
                break;
            case "closeWindow":
                browser_api.storage.local.get(['setGmail'], function (settings) {
                    if (settings.setGmail && settings.setGmail <= 3)
                        gmailwindow.close();
                })
                break;
            default:
                break;
        }
        return;
    }
    var event = new CustomEvent('addon_reply', { detail: JSON.stringify(reply) });
    document.dispatchEvent(event)
});

/*var g_id = 0;
function messageAddon(data) {
    var id = g_id++;
    document.dispatchEvent(new CustomEvent("addon", { "detail": { id: id, data: data } }));
    return new Promise(function (resolve, reject) {
        document.addEventListener("addon_reply", function (event) {
            resolve(JSON.parse(event.detail).data);
        });
    });
}

await messageAddon({ task: "version" })*/